import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:navtaxidriver/model/user_response.dart';
import 'package:navtaxidriver/model/vehicle_type_response.dart';

class Api {
  static const String baseUrl = "https://navtark.dev/navtaxi/api/";
  static const String phoneUrl = "driver_number";
  static const String profilePicUrl = "uploadprofilepic";
  static const String uploadDocument = "uploaddocument";
  static const String vehiclesType = "vehicles_type";
  static const String vehiclesManufacturer = "vehicles_manufacturer";
  static const String register = "post_driver_details";
  static const String session = "get_session";
  static const String updateLatLong = "update_driver_latlong";
  static String authToken = "";

  static Future<String> calPostApi(String url, String body) async {
    log("Url : $url");
    log("Body : $body");
    Map<String, String> map = <String, String>{
      HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
    };
    if (authToken.isNotEmpty) {
      // map[HttpHeaders.authorizationHeader] = 'bearer $authToken';
      map["Api-Key"] = authToken;
    }
    http.Response response =
        await http.post(Uri.parse(baseUrl + url), headers: map, body: body);
    if (response.statusCode == 200) {
      log("Response ${response.body}");
      return response.body;
    } else {
      log("statusCode : ${response.statusCode}");
      log("body ${response.body}");
      if (response.body.contains("error")) {
        throw Exception(json.decode(response.body)["error"]);
      } else {
        throw Exception(response.body);
      }
    }
  }

  static Future<http.Response> getMultipartApi(
      String url, List<http.MultipartFile> list, Map<String, String> body) async {
    log("Url: $url");
    log("Body : $body");
    log("MultiPart List : $list");
    var request = http.MultipartRequest('POST', Uri.parse(baseUrl + url));
    request.files.addAll(list);
    request.fields.addAll(body);
    Map<String, String> map = <String, String>{
      HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
      HttpHeaders.authorizationHeader: "bearer $authToken"
    };
    request.headers.addAll(map);
    var stream = await request.send();
    var response = await http.Response.fromStream(stream);
    if (response.statusCode != 200) {
      log("statusCode : ${response.statusCode}");
      log("body ${response.body}");
      if (response.body.contains("error")) {
        throw Exception(json.decode(response.body)["error"]);
      } else {
        throw Exception(response.body);
      }
    }
    return response;
  }

  static Future<void> verifyPhone(
      String phoneNumber, BuildContext context, codeResend, autoRetrieval, onError,
      {int? ftr}) async {
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        verificationCompleted: (phoneAuthCredential) {
          log("Verification complete code ${phoneAuthCredential.smsCode} ");
        },
        verificationFailed: onError,
        codeSent: codeResend,
        codeAutoRetrievalTimeout: /*(verificationId) {
          // showSnackBar("codeAutoRetrievalTimeout $verificationId");
        }*/
            autoRetrieval,
        forceResendingToken: ftr);
  }

  ///Check Mobile Number
  static Future<UserResponse> callPhoneApi(
      String phone, String code, String deviceToken) async {
    String _body = jsonEncode(<String, String>{
      'country_code': code,
      'mobile': phone,
      'device_type': Platform.isAndroid ? "ANDROID" : "IOS",
      'device_token': deviceToken,
    });
    String response;
    try {
      response = await calPostApi(phoneUrl, _body);
      return UserResponse.fromJson(jsonDecode(response));
    } on Exception catch (e) {
      log("Error on Api : $e");
      return UserResponse(success: false, message: "Something went wrong");
    }
  }

  ///Register Api
  static Future<UserResponse> callRegisterApi(String id,
      String name,
      String email,
      String vehicleName,
      String vehicleType,
      String vehicalNumber,
      String vehicalManufacture,
      ) async {
    String _body = jsonEncode(<String, String>{
      'id': id,
      'email': email,
      'name': name,
      'vehical_name': vehicleName,
      'vehical_type': vehicleType,
      'vehical_number': vehicalNumber,
      'vehical_manufacture': vehicalManufacture
    });
    // String url = baseUrl + phoneUrl;
    String response;
    try {
      response = await calPostApi(register, _body);
      return UserResponse.fromJson(jsonDecode(response));
    } on Exception catch (e) {
      log("Error on Api : $e");
      return UserResponse(success: false, message: "Something went wrong");
    }
  }

  ///Upload Profile Photo API
  static Future<UserResponse> uploadProfilePic(String id, String profilePicPath) async {
    // String url = baseUrl + profilePicUrl;
    // log(url);
    http.Response response = await getMultipartApi(profilePicUrl,
        [await http.MultipartFile.fromPath('profile_pic', profilePicPath)], {'id': id});
    log("uploadProfilePic Response ${response.body}");
    return UserResponse.fromJson(jsonDecode(response.body));
  }

  ///Upload Vehicle & Driver Document API
  static Future<UserResponse> uploadDocApi(String id, String type, String docPath) async {
    http.Response response = await getMultipartApi(uploadDocument, [
      await http.MultipartFile.fromPath('vehical_docs', docPath)
    ], {
      'id': id,
      'type': type,
    });
    log("upload Document Response ${response.body}");
    return UserResponse.fromJson(jsonDecode(response.body));
  }

  ///fetch Vehicle Type List
  static Future<VehicleTypeResponse> getTypeList() async {
    http.Response response = await http.get(Uri.parse(baseUrl + vehiclesType));
    log("Response ${response.body}");

    if (response.statusCode == 200) {
      return VehicleTypeResponse.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Unable to fetch Veihicle Type list");
    }
  }

  ///Get Manufacturer According to type
  static Future<VehicleTypeResponse> getManufactureById(String id) async {
    String _body = jsonEncode(<String, String>{
      'id': id,
    });
    // String url = baseUrl + phoneUrl;
    String response;
    try {
      response = await calPostApi(vehiclesManufacturer, _body);
      return VehicleTypeResponse.fromJson(jsonDecode(response));
    } on Exception catch (e) {
      log("Error on Api : $e");
      return VehicleTypeResponse(success: false, message: "Something went wrong");
    }
  }

  ///Get session
  static Future<UserResponse> callSessionApi(String id) async {
    String _body = jsonEncode(<String, String>{
      'driver_id': id,
    });
    // String url = baseUrl + phoneUrl;
    String response;
    try {
      response = await calPostApi(session, _body);
      return UserResponse.fromJson(jsonDecode(response));
    } on Exception catch (e) {
      log("Error on Api : $e");
      return UserResponse(success: false, message: "Something went wrong");
    }
  }

  ///Get session
  static Future<UserResponse> updateLatLongApi(String id,double latitude,double longitude) async {
    String _body = jsonEncode(<String, String>{
      'driver_id': id,
      'latitude': "$latitude",
      'longitude': "$longitude",
    });
    // String url = baseUrl + phoneUrl;
    String response;
    try {
      response = await calPostApi(updateLatLong, _body);
      return UserResponse.fromJson(jsonDecode(response));
    } on Exception catch (e) {
      log("Error on Api : $e");
      return UserResponse(success: false, message: "Something went wrong");
    }
  }
}
