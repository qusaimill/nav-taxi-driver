import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import '../router.dart';
import 'constants.dart';

double getWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double getHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

Widget getMainBackground(BuildContext context, Widget child) {
  return Stack(children: [
    getImage(
      "map_bg.png",
      width: getWidth(context),
      height: getHeight(context),
      fit: BoxFit.fill,
    ),
    Container(
      width: getWidth(context),
      height: getHeight(context),
      color: const Color(0x99ebb000),
    ),
    child
  ]);
}

Text getText(String txt, {TextStyle? style}) {
  return Text(
    txt,
    style: style,
  );
}


TextStyle getTitleStyle() {
  return const TextStyle(fontSize: 28, fontWeight: FontWeight.bold);
}

Image getImage(String name, {double? width, double? height, BoxFit? fit}) {
  return Image.asset(
    imagePath + name,
    width: width,
    height: height,
    fit: fit,
  );
}

SizedBox getSizeBox(double height) {
  return SizedBox(
    height: height,
  );
}

showSnackBar(String value) {
  WidgetsBinding.instance!.addPostFrameCallback((_) {
    rootScaffoldMessengerKey.currentState!.hideCurrentSnackBar();
    rootScaffoldMessengerKey.currentState!.showSnackBar(getSnackBar(value));
  });
}

getSnackBar(String value) {
  return SnackBar(content: Text(value));
}

void hideKeyboard(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
}

Widget getMenu(void Function() onPress) {
  return Padding(
    padding: const EdgeInsets.only(top: 10, left: 10),
    child: InkWell(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: const [
            BoxShadow(
              color: Colors.black,
              blurRadius: 7, // Shadow position
            ),
          ],
        ),
        padding: EdgeInsets.all(5),
        child: const Icon(
          Icons.menu,
          size: 35,
        ),
      ),
    ),
  );
}

Widget backButton(BuildContext context) {
  return IconButton(
    onPressed: () {
      close(context);
    },
    icon: Icon(Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios_new),
    iconSize: 30,
  );
}

Widget getButton(String text, onPress) {
  return ElevatedButton(
      style: ElevatedButton.styleFrom(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 12),
          primary: Colors.black,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      onPressed: onPress,
      child: getText(text.toUpperCase(),
          style: const TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold)));
}

Widget getLoading({Color? color}) {
  return CircularProgressIndicator(
    color: color ?? Colors.black,
  );
}

Widget baseLoader(BuildContext context) {
  return Container(
    height: getHeight(context),
    width: getWidth(context),
    color: const Color(0x88000000),
    child: Center(
      child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: getLoading(color: Colors.amber),
          )),
    ),
  );
}

