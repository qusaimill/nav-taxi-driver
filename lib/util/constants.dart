import 'package:flutter/material.dart';

final GlobalKey<ScaffoldMessengerState> rootScaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

const String imagePath = "images/"; //assets/images/
const String fontFamily = "Raleway";
const black = Colors.black;
const yellow = Color(0xffebb000);//#F6D000
const yellow_accent = Color(0xffF6D000);//#F6D000
const yellowAlpha = Color(0xffE2D9A8);//#F6D000
const whiteLight = Color(0xffB9BFC5);
const grey = Color(0xff797979);

///Share Pref Keys
const String keyToken = "token";
const String keyLogin = "isLogin";
const String keyVerify = "isVerify";
const String keyPhoneNumber = "phone";
const String keyDriverId = "driverId";
const String keyUser = "userResponse";
const String keyDeviceToken = "deviceToken";

///Email Regex
final RegExp emailReg= RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
