/// success : true
/// message : "successfully"
/// data : {"details":{"id":1,"name":"","email":"","mobile":"9425650616","vehical_name":"","vehical_type":"","vehical_number":"","api_key":"MTYzOTU0NzM3Nw==","licence":"http://localhost/navtaxi/images/doc/1639477575Screenshot.png","insurance":"http://localhost/navtaxi/images/doc/1639476700Screenshot.png","vehical_documents":""}}

class UserResponse {
  UserResponse({
    bool? success,
    String? message,
    String? error,
    Data? data,
  }) {
    _success = success;
    _message = message;
    _data = data;
  }

  UserResponse.fromJson(dynamic json) {
    _success = json['success'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    if (_data == null || data!.details == null) {
      data!.details = json['data'] != null ? Details.fromJson(json['data']) : null;
    }
  }

  bool? _success;
  String? _message;
  Data? _data;

  // Details? _details;

  bool? get success => _success;

  String? get message => _message;

  Data? get data => _data;

  // Details? get details => _details;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['message'] = _message;
    if (_data != null && _data!.details != null) {
      map['data'] = _data?.toJson();
    }
    /*else if (_details != null) {
      map['details'] = details?.toJson();
    }*/
    return map;
  }

  @override
  String toString() {
    // TODO: implement toString
    return toJson().toString();
  }
}

/// details : {"id":1,"name":"","email":"","country_code":"+91","mobile":"9425650616","vehical_name":"","vehical_type":"","vehical_number":"","api_key":"MTYzOTU0NzM3Nw==","licence":"http://localhost/navtaxi/images/doc/1639477575Screenshot.png","insurance":"http://localhost/navtaxi/images/doc/1639476700Screenshot.png","vehical_documents":""}

class Data {
  Data({
    Details? details,
  }) {
    _details = details;
  }

  Data.fromJson(dynamic json) {
    _details = json['details'] != null ? Details.fromJson(json['details']) : null;
  }

  Details? _details;

  Details? get details => _details;

  set details(Details? value) {
    _details = value;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_details != null) {
      map['details'] = _details?.toJson();
    }
    return map;
  }
}

/// id : 1
/// name : ""
/// email : ""
/// mobile : "9425650616"
/// vehical_name : ""
/// vehical_type : ""
/// vehical_number : ""
/// api_key : "MTYzOTU0NzM3Nw=="
/// licence : "http://localhost/navtaxi/images/doc/1639477575Screenshot.png"
/// insurance : "http://localhost/navtaxi/images/doc/1639476700Screenshot.png"
/// vehical_documents : ""
///"message": [""]
class Details {
  Details({
    int? id,
    String? name,
    String? email,
    String? countryCode,
    String? mobile,
    String? avatar,
    String? vehicalName,
    String? vehicalType,
    String? vehicalNumber,
    String? apiKey,
    String? status,
    dynamic latitude,
    dynamic longitude,
    String? insurance,
    String? licenceFront,
    String? licenceBack,
    String? vehicalDocumentsFront,
    String? vehicalDocumentsBack,
    List<String>? message,
  }) {
    _id = id;
    _name = name;
    _email = email;
    _countryCode = countryCode;
    _mobile = mobile;
    _avatar = avatar;
    _vehicalName = vehicalName;
    _vehicalType = vehicalType;
    _vehicalNumber = vehicalNumber;
    _apiKey = apiKey;
    _status = status;
    _latitude = latitude;
    _longitude = longitude;
    _insurance = insurance;
    _licenceFront = licenceFront;
    _licenceBack = licenceBack;
    _vehicalDocumentsFront = vehicalDocumentsFront;
    _vehicalDocumentsBack = vehicalDocumentsBack;
    _message = message ?? [];
  }

  Details.fromJson(dynamic json) {
    _id = json['id'] ?? 0;
    _name = json['name'] ?? "";
    _email = json['email'] ?? "";
    _countryCode = json['country_code'] ?? "";
    _mobile = json['mobile'] ?? "";
    _avatar = json['avatar'] ?? "";
    _vehicalName = json['vehical_name'] ?? "";
    _vehicalType = json['vehical_type'] ?? "";
    _vehicalNumber = json['vehical_number'] ?? "";
    _apiKey = json['api_key'] ?? "";
    _status = json['status'] ?? "";
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _insurance = json['insurance'] ?? "";
    _licenceFront = json['licence_front'] ?? "";
    _licenceBack = json['licence_back'] ?? "";
    _vehicalDocumentsFront = json['vehical_documents_front'] ?? "";
    _vehicalDocumentsBack = json['vehical_documents_back'] ?? "";
    _message = json['message'] ?? [];
  }

  int? _id;
  String? _name;
  String? _email;
  String? _countryCode;
  String? _mobile;
  String? _avatar;
  String? _vehicalName;
  String? _vehicalType;
  String? _vehicalNumber;
  String? _apiKey;
  String? _status;
  double? _latitude;
  double? _longitude;
  String? _insurance = "";
  String? _licenceFront;
  String? _licenceBack;
  String? _vehicalDocumentsFront = "";
  String? _vehicalDocumentsBack = "";
  List<dynamic>? _message = [];

  int? get id => _id;

  String? get name => _name;

  String? get email => _email;

  String? get countryCode => _countryCode;

  String? get mobile => _mobile;

  String? get avatar => _avatar;

  String? get vehicalName => _vehicalName;

  String? get vehicalType => _vehicalType;

  String? get vehicalNumber => _vehicalNumber;

  String? get apiKey => _apiKey;

  String? get status => _status;

  double? get latitude => _latitude;

  double? get longitude => _longitude;

  String? get insurance => _insurance;

  String? get licenceFront => _licenceFront;

  String? get licenceBack => _licenceBack;

  String? get vehicalDocumentsFront => _vehicalDocumentsFront;

  String? get vehicalDocumentsBack => _vehicalDocumentsBack;

  List<dynamic>? get message => _message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['email'] = _email;
    map['country_code'] = _countryCode;
    map['mobile'] = _mobile;
    map['avatar'] = _avatar;
    map['vehical_name'] = _vehicalName;
    map['vehical_type'] = _vehicalType;
    map['vehical_number'] = _vehicalNumber;
    map['api_key'] = _apiKey;
    map['status'] = _status;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['insurance'] = _insurance;
    map['licence_front'] = _licenceFront;
    map['licence_back'] = _licenceBack;
    map['vehical_documents_front'] = _vehicalDocumentsFront;
    map['vehical_documents_back'] = _vehicalDocumentsBack;
    map['message'] = _message;
    return map;
  }

  set licenceFront(String? value) {
    _licenceFront = value;
  }

  set licenceBack(String? value) {
    _licenceBack = value;
  }

  set avatar(String? value) {
    _avatar = value;
  }

  set status(String? value) {
    _status = value;
  }

  set name(String? value) {
    _name = value;
  }

  set email(String? value) {
    _email = value;
  }

  set vehicalDocumentsFront(String? value) {
    _vehicalDocumentsFront = value;
  }

  set vehicalDocumentsBack(String? value) {
    _vehicalDocumentsFront = value;
  }

  set insurance(String? value) {
    _insurance = value;
  }

  set vehicalName(String? value) {
    _vehicalName = value;
  }

  set message(List<dynamic>? value) {
    _message = value;
  }

  set vehicalType(String? value) {
    _vehicalType = value;
  }

  set vehicalNumber(String? value) {
    _vehicalNumber = value;
  }

  bool isVerify() {
    return name!.isNotEmpty &&
        email!.isNotEmpty &&
        licenceFront!.isNotEmpty &&
        _licenceBack!.isNotEmpty &&
        insurance!.isNotEmpty &&
        vehicalDocumentsFront!.isNotEmpty &&
        status!.isNotEmpty &&
        vehicalDocumentsBack!.isNotEmpty;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
