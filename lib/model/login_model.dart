import 'package:navtaxidriver/model/base_model.dart';
import 'package:navtaxidriver/util/util.dart';

class LoginModel extends BaseModel{
  String _phone="";

  String get phone => _phone;

  //Setters
  bool changePhoneNumber(String value){//[0-9]+
    if (value.length >= 10&&RegExp("^(?:[+]91)?[0-9]{10}\$").hasMatch(value)){
      _phone=value;
      return true;
    } else {
      showSnackBar("Please enter valid phone number");
      // _phone=ValidationItem(null, "Please enter valid phone number");
    }
    notifyListeners();
    return false;
  }
}