/// success : true
/// message : "successfully"
/// data : [{"id":1,"name":"auto"},{"id":2,"name":"bike"},{"id":3,"name":"car"}]

class VehicleTypeResponse {
  VehicleTypeResponse({
      bool? success, 
      String? message, 
      List<Type>? data,}){
    _success = success;
    _message = message;
    _type = type;
}

  VehicleTypeResponse.fromJson(dynamic json) {
    _success = json['success'];
    _message = json['message'];
    if (json['data'] != null) {
      _type = [];
      json['data'].forEach((v) {
        _type?.add(Type.fromJson(v));
      });
    }
  }
  bool? _success;
  String? _message;
  List<Type>? _type;

  bool? get success => _success;
  String? get message => _message;
  List<Type>? get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['message'] = _message;
    if (_type != null) {
      map['data'] = _type?.map((v) => v.toJson()).toList();
    }
    return map;
  }
@override
  String toString() {
    // TODO: implement toString
    return toJson().toString();
  }
}

/// id : 1
/// name : "auto"

class Type {
  Type({
      int? id, 
      String? name,}){
    _id = id;
    _name = name;
}

  Type.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
  }
  int? _id;
  String? _name;

  int? get id => _id;
  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }

}