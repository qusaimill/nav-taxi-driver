import 'dart:convert';
import 'dart:developer';

import 'package:navtaxidriver/api/api.dart';
import 'package:navtaxidriver/model/base_model.dart';
import 'package:navtaxidriver/model/user_response.dart';
import 'package:navtaxidriver/router.dart';
import 'package:navtaxidriver/util/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefer extends BaseModel {
  SharedPreferences? sp;
  // String mainRoute=splashScreenRoute;

  SharedPrefer() {
    setup();
  }

  setup() async {
    sp ??= await SharedPreferences.getInstance();
    notifyListeners();
  }

  Future<bool> saveString(String key, String value) async {
    sp ??= await SharedPreferences.getInstance();
    if (key.isNotEmpty) {
      return await sp!.setString(key, value);
    }
    return false;
  }

  saveBool(String key, bool value) async {
    sp ??= await SharedPreferences.getInstance();
    if (key.isNotEmpty) {
      await sp!.setBool(key, value);
    }
  }

  Future<String> getString(String key) async {
    sp ??= await SharedPreferences.getInstance();
    return sp!.getString(key) ?? "";
  }

  Future<bool> getBool(String key) async {
    sp ??= await SharedPreferences.getInstance();
    return sp!.getBool(key) ?? false;
  }

  Future<bool> saveSession(Details? user) async {
    if (user != null ) {
      Api.authToken=user.apiKey??"";
      await saveString(keyToken, user.apiKey!);
      await saveString(keyDriverId, "${user.id}");
    }
    await saveBool(keyLogin, true);
    return await saveString(keyUser, jsonEncode(user!.toJson()));
  }

  Future<Details> getUser() async {
    sp ??= await SharedPreferences.getInstance();
    String user=sp!.getString(keyUser)??"";
    return Details.fromJson(jsonDecode(user));
  }

  Future<bool> clearPref() async {
    sp ??= await SharedPreferences.getInstance();
    return sp!.clear();
  }
}
