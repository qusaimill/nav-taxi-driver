import 'package:flutter/material.dart';
import 'package:navtaxidriver/model/user_response.dart';
import 'package:navtaxidriver/src/approval.dart';
import 'package:navtaxidriver/src/home/home.dart';
import 'package:navtaxidriver/src/login.dart';
import 'package:navtaxidriver/src/otp.dart';
import 'package:navtaxidriver/src/registration.dart';
import 'package:navtaxidriver/src/splash.dart';
import 'package:navtaxidriver/undefined_screen.dart';

/*---------------Routes Names------------*/
const String splashScreenRoute = "/";
const String loginScreenRoute = "/login";
const String otpScreenRoute = "/otp";
const String approvalScreenRoute = "/approval";
const String registerScreenRoute = "/register";
const String registerScreenSecondRoute = "/register2";
const String homeScreenRoute = "home";
String mainRoute = splashScreenRoute;
/*---------------Routes Names------------*/

open(BuildContext context, String name, {Object? arguments}) {
  Navigator.of(context).pushNamed(name, arguments: arguments);
}

openUntil(BuildContext context, String name, {Object? arguments}) {
  Navigator.of(context)
      .pushNamedAndRemoveUntil(name, ModalRoute.withName("/"), arguments: arguments);
}

close(BuildContext context) {
  Navigator.of(context).pop();
}

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case splashScreenRoute:
      bool isOtp=false;
      if(settings.arguments!=null) {
        SplashArgument arg = settings.arguments as SplashArgument;
        isOtp=arg.isOtp;
      }
      return MaterialPageRoute(
        builder: (context) => SplashScreen(isFromOTP: isOtp,),
      );
    case loginScreenRoute:
      return MaterialPageRoute(
        builder: (context) => LoginScreen(),
      );
    case otpScreenRoute:
      OtpScreenArguments arg = settings.arguments as OtpScreenArguments;
      return MaterialPageRoute(
        builder: (context) =>
            OTPScreen(arg.phone, arg.countryCode /*,arg.verificationId,arg.ftr*/),
      );
    case approvalScreenRoute:
      ApprovalScreenArgument arg =
      settings.arguments as ApprovalScreenArgument;
      return MaterialPageRoute(
        builder: (context) => ApprovalScreen(arg.msg),
      );
    case registerScreenRoute:
      RegisterScreenArgument arg = settings.arguments as RegisterScreenArgument;
      return MaterialPageRoute(
        builder: (context) => RegisterScreen(arg.phone),
      );
    case registerScreenSecondRoute:
      RegisterSecondScreenArgument arg =
          settings.arguments as RegisterSecondScreenArgument;
      return MaterialPageRoute(
        builder: (context) => RegisterSecondTab(arg.details),
      );
    case homeScreenRoute:
      return MaterialPageRoute(
        builder: (context) => const HomeScreen(),
      );
    default:
      return MaterialPageRoute(
        builder: (context) => UndefinedScreen(name: settings.name),
      );
  }
}
class SplashArgument{
  bool isOtp;

  SplashArgument(this.isOtp);
}
class OtpScreenArguments {
  final String phone;

  // final String verificationId;
  // final int ftr;
  final String countryCode;

  OtpScreenArguments(this.phone, this.countryCode /*,this.verificationId,this.ftr*/);
}

class RegisterScreenArgument {
  final String phone;

  RegisterScreenArgument(this.phone);
}

class RegisterSecondScreenArgument {
  final Details details;

  RegisterSecondScreenArgument(this.details);
}

class ApprovalScreenArgument {
  final String msg;

  ApprovalScreenArgument(this.msg);
}
