import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:navtaxidriver/api/api.dart';
import 'package:navtaxidriver/model/shared_pre.dart';
import 'package:navtaxidriver/model/user_response.dart';
import 'package:navtaxidriver/model/vehicle_type_response.dart';
import 'package:navtaxidriver/router.dart';
import 'package:navtaxidriver/util/util.dart';
import 'package:navtaxidriver/util/constants.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  String phone;

  RegisterScreen(this.phone, {Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _name = TextEditingController();

  final TextEditingController _phone = TextEditingController();

  final TextEditingController _email = TextEditingController();

  late Details user;

  @override
  void initState() {
    super.initState();
    initSp();
  }

  @override
  Widget build(BuildContext context) {
    // _phone.value = TextEditingValue(text: widget.phone);
    return SafeArea(
      child: Scaffold(
          body: Stack(children: [
        getImage(
          "map_bg.png",
          width: getWidth(context),
          height: getHeight(context),
          fit: BoxFit.fill,
        ),
        Container(
          width: getWidth(context),
          height: getHeight(context),
          color: const Color(0x99ffffff),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(),
                Align(alignment: Alignment.topLeft, child: backButton(context)),
                getProfileView(),
                getSizeBox(10),
                getTextField("Name", _name, Icons.person, TextInputType.name),
                getTextField("Phone", _phone, Icons.phone, null),
                getTextField("Email", _email, Icons.mail, TextInputType.emailAddress),
                getSizeBox(10),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: getText("Driving License", style: getTitleStyle())),
                ),
                getSizeBox(10),
                uploadLicence(),
                getSizeBox(10),
                getButton("Register", () {
                  // log(_phone.text);
                  if (profilePic == null || profilePic!.isEmpty) {
                    showSnackBar("Please upload profile pic.");
                    return;
                  } else if (_name.text.isEmpty) {
                    showSnackBar("Please enter name.");
                    return;
                  } else if (_name.text.length < 4) {
                    showSnackBar("Please enter at least 4 char name!");
                    return;
                  } else if (_email.text.isEmpty) {
                    showSnackBar("Please enter email address.");
                    return;
                  } else if (!emailReg.hasMatch(_email.text)) {
                    showSnackBar("Please enter valid email address!");
                    return;
                  } else if (licenceFront == null || licenceFront!.isEmpty) {
                    showSnackBar("Please upload licence front image!");
                    return;
                  } else if (licenceBack == null || licenceBack!.isEmpty) {
                    showSnackBar("Please upload licence back image!");
                    return;
                  }
                  user.name = _name.text;
                  user.email = _email.text;
                  hideKeyboard(context);
                  open(context, registerScreenSecondRoute,
                      arguments: RegisterSecondScreenArgument(user));
                })
              ],
            ),
          ),
        )
      ])),
    );
  }

  /* _imgFromCamera(bool isFront) async {
    XFile? image =
        await ImagePicker().pickImage(source: ImageSource.camera, imageQuality: 90);
    if (image != null) callUploadDocApi(isFront, image);
  }*/

  _getImg(bool fromCamera, bool isFront) async {
    XFile? image = await ImagePicker().pickImage(
        source: fromCamera ? ImageSource.camera : ImageSource.gallery, imageQuality: 90);
    if (image != null) {
      setState(() {
        if (isFront) {
          licenceFront = image.path;
        } else {
          licenceBack = image.path;
        }
        Future<UserResponse> future = Api.uploadDocApi(
            "${user.id}",
            isFront ? "licence_front" : "licence_back",
            isFront ? licenceFront! : licenceBack!);
        if (isFront) {
          futureLicenceFront = future;
        } else {
          futureLicenceBack = future;
        }
      });
      // callUploadDocApi(isFront, image);
    }
  }

  /*_imgFromGallery(bool isFront) async {
    XFile? image =
        await ImagePicker().pickImage(source: ImageSource.gallery, imageQuality: 90);
    if (image != null) callUploadDocApi(isFront, image);
  }*/
/*
  callUploadDocApi(bool isFront, XFile image) {

  }*/

  void _showPicker(context, bool isFront) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.photo_library),
                    title: getText('Photo Library'),
                    onTap: () {
                      _getImg(false, isFront);
                      Navigator.of(context).pop();
                    }),
                ListTile(
                  leading: const Icon(Icons.photo_camera),
                  title: getText('Camera'),
                  onTap: () {
                    _getImg(true, isFront);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  String? profilePic;

  // String? licenceFrontPath;
  // String? licenceBackPath;
  String? licenceFront;
  String? licenceBack;
  Future<UserResponse>? futureProfile;
  Future<UserResponse>? futureLicenceFront;
  Future<UserResponse>? futureLicenceBack;

  getProfileView() {
    return InkWell(
      onTap: () {
        getCameraPic();
      },
      child: Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
            border: Border.all(width: 1.0, color: const Color(0xff000000)),
            boxShadow: const [
              BoxShadow(
                color: Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: FutureBuilder<UserResponse>(
            future: futureProfile,
            builder: (context, snapshot) {
              log(" ${snapshot.toString()}");
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: getLoading());
              }
              if (snapshot.hasData) {
                Details details;
                // if(snapshot.data!.data!.details != null){
                details = snapshot.data!.data!.details!;
                user.avatar = details.avatar;
                user.status = details.status;
                profilePic = details.avatar;
                sp.saveSession(user);
                /* }else{
                  details=snapshot.data!.details!;
                }*/
                return ClipRRect(
                  child: Image.network(
                    details.avatar!,
                    fit: BoxFit.cover,
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(50)),
                ); /*profilePic == null || profilePic!.path.isEmpty
                ? Center(
                    child: getText(
                    "Edit",
                    style: const TextStyle(color: Colors.black),
                  ))
                : ClipRRect(
                    child: Image.network(
                      snapshot.data!.data!.details!.avatar!,
                      fit: BoxFit.fill,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                  )*/
              }

              return profilePic == null || profilePic!.isEmpty
                  ? Center(
                      child: getText(
                      "Edit",
                      style: const TextStyle(color: Colors.black),
                    ))
                  : ClipRRect(
                      child: Image.network(
                        profilePic!,
                        fit: BoxFit.fill,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                    );
            },
          )),
    );
  }

  getCameraPic() async {
    final ImagePicker _picker = ImagePicker();
    var image = await _picker.pickImage(
        source: ImageSource.camera,
        preferredCameraDevice: CameraDevice.front,
        maxWidth: 100,
        maxHeight: 100);
    if (image != null) {
      log(image.path);
      setState(() {
        profilePic = image.path;
        futureProfile = Api.uploadProfilePic("${user.id}", profilePic!);
      });
    }
  }

  Widget uploadLicence() {
    return Container(
      margin: const EdgeInsets.all(5),
      decoration:
          BoxDecoration(border: Border.all(), borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              _showPicker(context, true);
            },
            child: FutureBuilder<UserResponse>(
              future: futureLicenceFront,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  Details details = snapshot.data!.data!.details!;
                  user.licenceFront = licenceFront = details.licenceFront;
                  sp.saveSession(user);
                  return Card(
                    color: yellowAlpha,
                    child: ClipRRect(
                      child: Image.network(
                        details.licenceFront!,
                        height: 65,
                        width: 65,
                        fit: BoxFit.fill,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Card(
                    color: yellowAlpha,
                    child: SizedBox(
                        width: 65, height: 65, child: Center(child: getLoading())),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                return licenceFront == null || licenceFront!.isEmpty
                    ? Card(
                        color: yellowAlpha,
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.add_rounded,
                            size: 30,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      )
                    : Card(
                        color: yellowAlpha,
                        child: ClipRRect(
                          child: Image.network(
                            licenceFront!,
                            height: 65,
                            width: 65,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      );
              },
            ),
          ),
          Expanded(
              child: getText("Front",
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
          InkWell(
            onTap: () {
              _showPicker(context, false);
            },
            child: FutureBuilder<UserResponse>(
              future: futureLicenceBack,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  Details details = snapshot.data!.data!.details!;
                  user.licenceBack = licenceBack = details.licenceBack;
                  sp.saveSession(user);
                  return Card(
                    color: yellowAlpha,
                    child: ClipRRect(
                      child: Image.network(
                        details.licenceBack!,
                        height: 65,
                        width: 65,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Card(
                    color: yellowAlpha,
                    child: SizedBox(
                        width: 65, height: 65, child: Center(child: getLoading())),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                return licenceBack == null || licenceBack!.isEmpty
                    ? Card(
                        color: yellowAlpha,
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.add_rounded,
                            size: 30,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      )
                    : Card(
                        color: yellowAlpha,
                        child: ClipRRect(
                          child: Image.network(
                            licenceBack!,
                            height: 65,
                            width: 65,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      );
              },
            ),
          ),
          Expanded(
              child: getText("Back",
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
        ],
      ),
    );
  }

/*
  Future<void> getLicence(bool isFront) async {
    final ImagePicker _picker = ImagePicker();
    if (isFront) {
      licenceFront = await _picker.pickImage(source: ImageSource.gallery);
    } else {
      licenceBack = await _picker.pickImage(source: ImageSource.gallery);
    }
    setState(() {});
    log(licenceFront!.path);
  }*/
  late SharedPrefer sp;

  Future<void> initSp() async {
    sp = Provider.of<SharedPrefer>(context, listen: false);
    user = await sp.getUser();
    setState(() {
      _name.text = user.name!;
      _email.text = user.email!;
      _phone.text = user.countryCode! + user.mobile!;
      licenceFront = user.licenceFront;
      licenceBack = user.licenceBack;
      profilePic = user.avatar;
    });
    if (profilePic != null &&
        profilePic!.isNotEmpty &&
        _name.text.isNotEmpty &&
        _email.text.isNotEmpty &&
        emailReg.hasMatch(_email.text) &&
        _phone.text.isNotEmpty &&
        licenceFront != null &&
        licenceFront!.isNotEmpty &&
        licenceBack != null &&
        licenceBack!.isNotEmpty) {
      open(context, registerScreenSecondRoute,
          arguments: RegisterSecondScreenArgument(user));
    }
    if (user.message != null && user.message!.isNotEmpty) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: getText("Some Documents is Decline"),
            content:    ListView.builder(
              itemBuilder: (context, index) {
                return getText(user.message![index],
                    style: const TextStyle(fontWeight: FontWeight.bold));
              },
              itemCount: user.message!.length,
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: getText("OK"),
              ),
            ],
          );



        },
      );
    }
    log("User responce $user");
  }
}

getTextField(String label, TextEditingController textEditingController, IconData icon,
    TextInputType? type) {
  return Padding(
    padding: const EdgeInsets.all(5.0),
    child: TextFormField(
      readOnly: type == null,
      textCapitalization:
          label.toLowerCase() == "name" || label.toLowerCase() == "vehicle name"
              ? TextCapitalization.sentences
              : TextCapitalization.none,
      controller: textEditingController,
      cursorColor: Colors.black,
      keyboardType: type,
      style: const TextStyle(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.bold,
          fontFamily: fontFamily),
      decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: Colors.black,
          ),
          fillColor: yellow_accent,
          filled: true,
          labelStyle: const TextStyle(color: Colors.black),
          labelText: label,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(color: Colors.black)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(color: Colors.white))),
    ),
  );
}

class RegisterSecondTab extends StatefulWidget {
  // final String phone;
  final Details details;

  const RegisterSecondTab(this.details, {Key? key}) : super(key: key);

  @override
  State<RegisterSecondTab> createState() => _RegisterSecondTabState();
}

class _RegisterSecondTabState extends State<RegisterSecondTab> {
  final TextEditingController _vehicleName = TextEditingController();

  final TextEditingController _vehicleNumber = TextEditingController();

  final TextEditingController _vehicleType = TextEditingController();

  final TextEditingController _vehicleManufac = TextEditingController();

  late List<DropdownMenuItem<int>> menuItems;

  Future<UserResponse>? futureRegister;
  late Future<VehicleTypeResponse> futureType;
  Future<VehicleTypeResponse>? futureManufac;

  int selectedValue = 1;
  int selectedManValue = 1;

  @override
  void initState() {
    super.initState();
    init();
    futureType = Api.getTypeList();
  }

  void init() {
    _vehicleName.text = widget.details.vehicalName!;
    _vehicleNumber.text = widget.details.vehicalNumber!;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(),
                Align(alignment: Alignment.topLeft, child: backButton(context)),
                Container(
                    padding: const EdgeInsets.only(left: 10),
                    alignment: Alignment.topLeft,
                    child: getText("Vehicle Details", style: getTitleStyle())),
                getSizeBox(10),
                getTextField("Vehicle name", _vehicleName, Icons.directions_car,
                    TextInputType.name),
                getTextField("Vehicle Number", _vehicleNumber, Icons.directions_car,
                    TextInputType.text),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  height: 60,
                  decoration: BoxDecoration(
                      color: yellow_accent,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.black)),
                  child: FutureBuilder<VehicleTypeResponse>(
                    future: futureType,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        List<Type>? a = snapshot.data!.type;
                        if (a == null || a.isEmpty) {
                          return InkWell(
                              onTap: () {
                                setState(() {
                                  futureType = Api.getTypeList();
                                });
                              },
                              child: Center(
                                  child: getText("No result found please try again!",
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))));
                        }
                        _vehicleType.text = a[0].name!;

                        menuItems = List.generate(a.length, (index) {
                          Type type = a[index];
                          return DropdownMenuItem(
                              child: Container(
                                  width: getWidth(context) / 1.15,
                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      const Icon(
                                        Icons.directions_car,
                                        color: Colors.black,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: getText(type.name!.toUpperCase(),
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold)),
                                      )
                                    ],
                                  )),
                              value: type.id!);
                        });
                        return DropdownButton(
                            iconEnabledColor: Colors.black,
                            iconSize: 30,
                            value: selectedValue,
                            onChanged: (int? newValue) {
                              hideKeyboard(context);
                              setState(() {
                                selectedValue = newValue!;
                                _vehicleType.text = a[newValue - 1].name!;
                                futureManufac = null;
                                _vehicleManufac.text = "";
                              });
                            },
                            items: menuItems);
                      }
                      if (snapshot.hasError) {
                        return getText(" ${snapshot.error}");
                      }
                      return Center(child: getLoading());
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  height: 60,
                  decoration: BoxDecoration(
                      color: yellow_accent,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.black)),
                  child: FutureBuilder<VehicleTypeResponse>(
                    future: futureManufac,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(child: getLoading());
                      }

                      if (snapshot.connectionState == ConnectionState.none) {
                        return InkWell(
                            onTap: () {
                              setState(() {
                                futureManufac = Api.getManufactureById("$selectedValue");
                              });
                            },
                            child: Center(
                                child: getText("Choose Manufacture",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black))));
                      }
                      if (snapshot.hasData) {
                        List<Type>? a = snapshot.data!.type;
                        if (a == null || a.isEmpty) {
                          return InkWell(
                              onTap: () {
                                setState(() {
                                  futureManufac =
                                      Api.getManufactureById("$selectedValue");
                                });
                              },
                              child: Center(
                                  child: getText("there is no Manufacture found!",
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))));
                        }
                        _vehicleManufac.text = a[0].name!;

                        menuItems = List.generate(a.length, (index) {
                          Type type = a[index];
                          return DropdownMenuItem(
                              child: Container(
                                  width: getWidth(context) / 1.15,
                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      const Icon(
                                        Icons.directions_car,
                                        color: Colors.black,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: getText(type.name!.toUpperCase(),
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold)),
                                      )
                                    ],
                                  )),
                              value: type.id!);
                        });
                        return DropdownButton(
                            iconEnabledColor: Colors.black,
                            iconSize: 30,
                            value: selectedManValue,
                            onChanged: (int? newValue) {
                              setState(() {
                                selectedManValue = newValue!;
                                _vehicleManufac.text = a[newValue - 1].name!;
                              });
                            },
                            items: menuItems);
                      }
                      if (snapshot.hasError) {
                        return Center(
                          child: Text(
                            "${snapshot.error}",
                            maxLines: 1,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                            textAlign: TextAlign.center,
                          ),
                        );
                      }

                      return Center(child: getLoading());
                    },
                  ),
                ),
                uploadSingleDoc("Vehicle Insurance"),
                getSizeBox(10),
                Container(
                    padding: const EdgeInsets.only(left: 10),
                    alignment: Alignment.topLeft,
                    child: getText("Vehicle Documents",
                        style:
                            const TextStyle(fontSize: 18, fontWeight: FontWeight.bold))),
                getSizeBox(5),
                uploadDoc(),
                /*  DropdownButton<String>(
                  items: <String>['Car', 'Bike', 'Auto'].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (_) {},
                ),*/
                getSizeBox(10),
                FutureBuilder<UserResponse>(
                  future: futureRegister,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(child: getLoading());
                    }
                    if (snapshot.connectionState == ConnectionState.none ||
                        snapshot.hasData) {
                      if (snapshot.hasData) {
                        SharedPrefer sp =
                            Provider.of<SharedPrefer>(context, listen: false);
                        Details details = snapshot.data!.data!.details!;
                        widget.details.vehicalName = details.vehicalName!;
                        sp.saveSession(widget.details);
                      }
                      return SubmitButon();
                    }
                    if (snapshot.hasError) {
                      return Column(
                        children: [
                          SubmitButon(),
                          getText("${snapshot.error}",
                              style: const TextStyle(color: Colors.red)),
                        ],
                      );
                    }
                    return Center(child: getLoading());
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? vehicleFront;
  String? vehicleBack;
  String? insurance;
  Future<UserResponse>? futureVehicleFront;
  Future<UserResponse>? futureVehicleBack;
  Future<UserResponse>? futureInsurance;

  _imgFromCamera(bool isFront, bool? isInsure) async {
    XFile? image =
        await ImagePicker().pickImage(source: ImageSource.camera, imageQuality: 90);
    if (image != null) callUploadDocApi(isFront, isInsure, image);
  }

  _imgFromGallery(bool isFront, bool? isInsure) async {
    XFile? image =
        await ImagePicker().pickImage(source: ImageSource.gallery, imageQuality: 90);
    if (image != null) callUploadDocApi(isFront, isInsure, image);
  }

  callUploadDocApi(bool isFront, bool? isInsure, XFile image) {
    setState(() {
      if (isInsure == null) {
        if (isFront) {
          vehicleFront = image.path;
        } else {
          vehicleBack = image.path;
        }
      } else {
        insurance = image.path;
      }
      Future<UserResponse> future = Api.uploadDocApi(
          "${widget.details.id}",
          isInsure == null
              ? (isFront ? "vehical_documents_front" : "vehical_documents_back")
              : "insurance",
          isInsure == null
              ? isFront
                  ? vehicleFront!
                  : vehicleBack!
              : insurance!);
      if (isInsure == null) {
        if (isFront) {
          futureVehicleFront = future;
        } else {
          futureVehicleBack = future;
        }
      } else {
        futureInsurance = future;
      }
    });
  }

  void _showPicker(context, bool isFront, bool? isInsurance) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.photo_library),
                    title: getText('Photo Library'),
                    onTap: () {
                      _imgFromGallery(isFront, isInsurance);
                      Navigator.of(context).pop();
                    }),
                ListTile(
                  leading: const Icon(Icons.photo_camera),
                  title: getText('Camera'),
                  onTap: () {
                    _imgFromCamera(isFront, isInsurance);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  Widget uploadDoc() {
    return Container(
      margin: const EdgeInsets.all(5),
      decoration:
          BoxDecoration(border: Border.all(), borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              _showPicker(context, true, null);
            },
            child: FutureBuilder<UserResponse>(
              future: futureVehicleFront,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Card(
                    color: yellowAlpha,
                    child: SizedBox(
                        width: 65, height: 65, child: Center(child: getLoading())),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                if (snapshot.hasData) {
                  Details details = snapshot.data!.data!.details!;
                  widget.details.vehicalDocumentsFront =
                      vehicleFront = details.vehicalDocumentsFront;

                  return Card(
                    color: yellowAlpha,
                    child: ClipRRect(
                      child: Image.network(
                        details.vehicalDocumentsFront!,
                        height: 65,
                        width: 65,
                        fit: BoxFit.fill,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }

                return widget.details.vehicalDocumentsFront == null ||
                        widget.details.vehicalDocumentsFront!.isEmpty
                    ? Card(
                        color: yellowAlpha,
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.add_rounded,
                            size: 30,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      )
                    : Card(
                        color: yellowAlpha,
                        child: ClipRRect(
                          child: Image.network(
                            widget.details.vehicalDocumentsFront!,
                            height: 65,
                            width: 65,
                            fit: BoxFit.fill,
                          ),
                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      );
              },
            ),
          ),
          Expanded(
              child: getText("Front",
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
          InkWell(
            onTap: () {
              _showPicker(context, false, null);
            },
            child: FutureBuilder<UserResponse>(
              future: futureVehicleBack,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Card(
                    color: yellowAlpha,
                    child: SizedBox(
                        width: 65, height: 65, child: Center(child: getLoading())),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                if (snapshot.hasData) {
                  Details details;

                  // if(snapshot.data!.data!.details != null){
                  details = snapshot.data!.data!.details!;
                  widget.details.vehicalDocumentsBack =
                      vehicleBack = details.vehicalDocumentsBack;
                  // user.data!.details!.licence=details.licence;
                  // sp.saveSession(user);
                  return Card(
                    color: yellowAlpha,
                    child: ClipRRect(
                      child: Image.network(
                        vehicleBack!,
                        height: 65,
                        width: 65,
                        fit: BoxFit.fill,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }

                return widget.details.vehicalDocumentsBack == null ||
                        widget.details.vehicalDocumentsBack!.isEmpty
                    ? Card(
                        color: yellowAlpha,
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.add_rounded,
                            size: 30,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      )
                    : Card(
                        color: yellowAlpha,
                        child: ClipRRect(
                          child: Image.network(
                            widget.details.vehicalDocumentsBack!,
                            height: 65,
                            width: 65,
                            fit: BoxFit.fill,
                          ),
                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      );
              },
            ),
          ),
          Expanded(
              child: getText("Back",
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
        ],
      ),
    );
    /*return Container(
    margin: const EdgeInsets.all(5),
    decoration:
        BoxDecoration(border: Border.all(), borderRadius: BorderRadius.circular(15)),
    child: Row(
      children: [
        Card(
          color: yellowAlpha,
          child: const Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.add_rounded,
              size: 30,
            ),
          ),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
        Expanded(
            child: getText("Front",
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
        Card(
          color: yellowAlpha,
          child: const Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.add_rounded,
              size: 30,
            ),
          ),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
        Expanded(
            child: getText("Back",
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
      ],
    ),
  );*/
  }

  Widget uploadSingleDoc(String text) {
    return Container(
      margin: const EdgeInsets.all(5),
      decoration:
          BoxDecoration(border: Border.all(), borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              _showPicker(context, false, true);
            },
            child: FutureBuilder<UserResponse>(
              future: futureInsurance,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Card(
                    color: yellowAlpha,
                    child: SizedBox(
                        width: 65, height: 65, child: Center(child: getLoading())),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }
                if (snapshot.hasData) {
                  Details details = snapshot.data!.data!.details!;
                  SharedPrefer prefer = Provider.of<SharedPrefer>(context, listen: false);
                  prefer.saveSession(details);
                  if (details.status != null &&
                      details.status!.isNotEmpty &&
                      details.status!.toLowerCase() == "pending") {
                    openUntil(context, approvalScreenRoute);
                  }
                  // sp.saveSession(user);
                  return Card(
                    color: yellowAlpha,
                    child: ClipRRect(
                      child: Image.network(
                        details.insurance!,
                        height: 65,
                        width: 65,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  );
                }

                return widget.details.insurance == null ||
                        widget.details.insurance!.isEmpty
                    ? Card(
                        color: yellowAlpha,
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.add_rounded,
                            size: 30,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      )
                    : Card(
                        color: yellowAlpha,
                        child: ClipRRect(
                          child: Image.network(
                            widget.details.insurance!,
                            height: 65,
                            width: 65,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      );
              },
            ),
          ),
          Expanded(
              child: getText(text,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
        ],
      ),
    );
  }

  Widget SubmitButon() {
    return getButton("Submit", () {
      if (_vehicleName.text.isEmpty) {
        showSnackBar("Please enter vehicle name");
        return;
      } else if (_vehicleNumber.text.isEmpty) {
        showSnackBar("Please enter vehicle umber");
        return;
      } else if (_vehicleType.text.isEmpty) {
        showSnackBar("Please enter vehicle type");
        return;
      } else if (_vehicleManufac.text.isEmpty) {
        showSnackBar("Please enter vehicle Manufacturer");
        return;
      } else if (insurance == null || insurance!.isEmpty) {
        showSnackBar("Please upload vehicle insurance");
        return;
      } else if (vehicleFront == null || vehicleFront!.isEmpty) {
        showSnackBar("Please upload front pic of vehicle document");
        return;
      } else if (vehicleBack == null || vehicleBack!.isEmpty) {
        showSnackBar("Please upload back pic of vehicle document");
        return;
      }
      setState(() {
        futureRegister = Api.callRegisterApi(
            "${widget.details.id}",
            widget.details.name!,
            widget.details.email!,
            _vehicleName.text,
            _vehicleType.text,
            _vehicleNumber.text,
            _vehicleManufac.text);
      });
      // openUntil(context, homeScreenRoute);
    });
  }
}

/*
class DropDownDemo extends StatefulWidget {
  @override
  _DropDownDemoState createState() => _DropDownDemoState();
}

class _DropDownDemoState extends State<DropDownDemo> {
  String _chosenValue="";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(0.0),
          child: DropdownButton<String>(
            value: _chosenValue,
            //elevation: 5,
            style: TextStyle(color: Colors.black),

            items: <String>[
              'Android',
              'IOS',
              'Flutter',
              'Node',
              'Java',
              'Python',
              'PHP',
            ].map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            hint: Text(
              "Please choose a langauage",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            onChanged: (String value) {
              setState(() {
                _chosenValue = value;
              });
            },
          ),
        ),
      ),
    );
  }
}*/
/*
const String _svg_b1bup2 =
    '<svg viewBox="2.0 4.9 15.6 5.1" ><path transform="translate(-76.24, -24.69)" d="M 92.97225189208984 34.73842620849609 C 92.72988891601562 34.73842620849609 92.48748779296875 34.64146041870117 92.311767578125 34.44755172729492 C 90.45748901367188 32.44784545898438 88.45172119140625 31.43585586547852 86.34290313720703 31.4297981262207 L 86.33074188232422 31.4297981262207 C 82.70098876953125 31.4297981262207 79.87108612060547 34.42332458496094 79.84078216552734 34.44755172729492 C 79.50144195556641 34.81113815307617 78.92577362060547 34.82931900024414 78.56217956542969 34.49603652954102 C 78.19862365722656 34.15668869018555 78.17438507080078 33.58707427978516 78.51979827880859 33.21741485595703 C 78.65306091308594 33.07200241088867 81.901123046875 29.62400054931641 86.33074188232422 29.62400054931641 C 88.96678161621094 29.62400054931641 91.42701721191406 30.8359317779541 93.63880920410156 33.21741485595703 C 93.97816467285156 33.58707427978516 93.95394134521484 34.15668869018555 93.59036254882812 34.49603652954102 C 93.41461181640625 34.65965270996094 93.19040679931641 34.73842620849609 92.97225189208984 34.73842620849609 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_n73fo =
    '<svg viewBox="0.0 0.0 19.7 6.1" ><path transform="translate(-77.94, -28.81)" d="M 96.70841217041016 34.90610504150391 C 96.46603393554688 34.90610504150391 96.22360992431641 34.80913543701172 96.04788208007812 34.62128829956055 C 93.59370422363281 31.97319412231445 90.92739868164062 30.62792205810547 88.12175750732422 30.62185096740723 L 88.10962677001953 30.62185096740723 C 83.28607940673828 30.62185096740723 79.5472412109375 34.57888031005859 79.50478363037109 34.62128829956055 C 79.16543579101562 34.98488616943359 78.59583282470703 35.00307083129883 78.22616577148438 34.66371154785156 C 77.86262512207031 34.32437133789062 77.83834838867188 33.7547492980957 78.18374633789062 33.38511276245117 C 78.35346984863281 33.20330810546875 82.48621368408203 28.80999946594238 88.10962677001953 28.80999946594238 C 91.4425048828125 28.80999946594238 94.56325531005859 30.3552360534668 97.36888885498047 33.38511276245117 C 97.71434783935547 33.7547492980957 97.69004821777344 34.32437133789062 97.32649993896484 34.66371154785156 C 97.15077209472656 34.82733154296875 96.92656707763672 34.90610504150391 96.70841217041016 34.90610504150391 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_g3r2ks =
    '<svg viewBox="63.0 262.9 22.9 24.1" ><path transform="translate(63.0, 262.85)" d="M 14.8158483505249 14.54080677032471 C 13.53665447235107 16.87911796569824 9.706934928894043 17.05989265441895 7.938465118408203 14.70193386077881 L 7.938465118408203 14.70193386077881 C 8.490621566772461 14.50543785095215 8.893439292907715 14.22444820404053 9.025092124938965 13.85306835174561 C 9.670539855957031 14.32569980621338 10.44570350646973 14.58843326568604 11.24550437927246 14.60565090179443 C 12.14938926696777 14.60565090179443 13.04344844818115 14.23034191131592 13.84711933135986 13.62316799163818 C 13.93750858306885 14.01616191864014 14.30102634429932 14.32466220855713 14.82960319519043 14.55063152313232 Z M 6.075677394866943 15.03597736358643 C 4.763079166412354 15.32679271697998 3.536940574645996 15.69620609283447 2.686109781265259 16.18941307067871 C 0.7211431860923767 17.33891868591309 0.8292163014411926 20.07415390014648 0.3556594252586365 22.28080940246582 C 0.2220416814088821 22.9037036895752 0.0884239673614502 23.52463531494141 4.76837158203125e-07 24.14752960205078 L 4.497808933258057 24.14752960205078 L 4.497808933258057 22.63647079467773 C 4.486676692962646 22.4675350189209 4.570486068725586 22.30644416809082 4.715212821960449 22.21859931945801 C 4.859940052032471 22.13075065612793 5.041528224945068 22.13075065612793 5.186254501342773 22.21859550476074 C 5.330982208251953 22.30644416809082 5.414791584014893 22.4675350189209 5.403658866882324 22.63647079467773 L 5.403658866882324 24.14556503295898 L 17.53339767456055 24.14556503295898 L 17.53339767456055 22.63647079467773 C 17.53339958190918 22.38578414916992 17.73661994934082 22.18256187438965 17.98730659484863 22.18256187438965 C 18.23799324035645 22.18256187438965 18.44121360778809 22.38578414916992 18.44121360778809 22.63647079467773 L 18.44121360778809 24.14556503295898 L 22.9488468170166 24.14556503295898 C 22.87221336364746 23.74078178405762 22.78378868103027 23.34189224243164 22.71697998046875 22.92924880981445 C 22.34756660461426 20.62434005737305 22.34560203552246 17.34874153137207 20.16252517700195 16.20316696166992 C 19.19182968139648 15.69227504730225 17.78884506225586 15.29338836669922 16.42908668518066 14.94165992736816 C 18.44317817687988 15.19317436218262 20.94851112365723 14.80607604980469 21.27469444274902 13.87271785736084 C 18.31348991394043 13.63298988342285 17.38405990600586 9.08802318572998 17.40371131896973 5.779019832611084 C 16.92032814025879 0.9117990136146545 12.55220603942871 -0.9274097681045532 9.172464370727539 0.4382420182228088 C 4.912416934967041 2.161517858505249 5.476362228393555 5.918533802032471 5.073544025421143 9.453508377075195 C 4.808273792266846 11.76430797576904 4.020321846008301 13.69980144500732 1.878508448600769 13.87271881103516 C 2.179148197174072 14.71961879730225 4.23646879196167 15.13619136810303 6.075677394866943 15.03597736358643 Z M 12.70154476165771 3.595943212509155 C 10.71299839019775 5.608068466186523 8.616378784179688 6.39601993560791 6.413650989532471 6.954070568084717 C 6.390458583831787 7.071108818054199 6.383187294006348 7.190744876861572 6.392036437988281 7.309730052947998 C 6.392036437988281 10.35149765014648 8.687117576599121 14.00240707397461 11.24746894836426 14.00240707397461 C 13.80781936645508 14.00240707397461 16.38782119750977 10.35149765014648 16.38782119750977 7.309730052947998 C 16.39162063598633 6.846989631652832 16.31857872009277 6.386828422546387 16.17167472839355 5.94800853729248 C 14.23814964294434 5.926393508911133 13.48556613922119 4.953735828399658 12.70154476165771 3.590048551559448 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_rkl4d6 =
    '<svg viewBox="63.0 343.5 26.6 19.5" ><path transform="translate(-441.21, 80.81)" d="M 527.0108032226562 262.6820068359375 L 508.0584411621094 262.6820068359375 C 505.9375 262.6820068359375 504.2120056152344 264.290283203125 504.2120056152344 266.2671508789062 L 504.2120056152344 278.606689453125 C 504.2120056152344 280.5835571289062 505.9375 282.1925048828125 508.0584411621094 282.1925048828125 L 527.0108032226562 282.1925048828125 C 529.1317749023438 282.1925048828125 530.8580322265625 280.5835571289062 530.8580322265625 278.606689453125 L 530.8580322265625 266.2671508789062 C 530.8580322265625 264.290283203125 529.1317749023438 262.6820068359375 527.0108032226562 262.6820068359375 Z M 527.0108032226562 264.3954772949219 C 527.1685791015625 264.3954772949219 527.3207397460938 264.4144592285156 527.468017578125 264.4464721679688 L 519.4722290039062 272.9028625488281 C 518.982177734375 273.4210815429688 518.276123046875 273.7178039550781 517.5349731445312 273.7178039550781 C 516.7938842773438 273.7178039550781 516.087890625 273.4210815429688 515.5977172851562 272.9028625488281 L 507.6019897460938 264.4464721679688 C 507.7492370605469 264.4144592285156 507.9013977050781 264.3954772949219 508.0584411621094 264.3954772949219 L 527.0108032226562 264.3954772949219 Z M 529.0188598632812 278.606689453125 C 529.0188598632812 279.6385803222656 528.1179809570312 280.4783630371094 527.0108032226562 280.4783630371094 L 508.0584411621094 280.4783630371094 C 506.9513549804688 280.4783630371094 506.0503845214844 279.6385803222656 506.0503845214844 278.606689453125 L 506.0503845214844 266.2671508789062 C 506.0503845214844 266.0162048339844 506.1051025390625 265.7770385742188 506.2011413574219 265.55810546875 L 514.2178955078125 274.0360412597656 C 515.0571899414062 274.9235229492188 516.2659301757812 275.4319458007812 517.5349731445312 275.4319458007812 C 518.8034057617188 275.4319458007812 520.0128173828125 274.9235229492188 520.85205078125 274.0360412597656 L 528.8681640625 265.55810546875 C 528.9649658203125 265.7770385742188 529.0188598632812 266.0162048339844 529.0188598632812 266.2671508789062 L 529.0188598632812 278.606689453125 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_pd7gsf =
    '<svg viewBox="339.0 504.0 15.0 8.0" ><path transform="matrix(-1.0, 0.0, 0.0, -1.0, 354.0, 512.0)" d="M 7.499999523162842 0 L 15.00000095367432 8 L 0 8 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_elc9zx =
    '<svg viewBox="18.0 7.5 1.0 21.0" ><path  d="M 18 7.5 L 18 28.5" fill="none" stroke="#000000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_ppmqyv =
    '<svg viewBox="7.5 18.0 21.0 1.0" ><path  d="M 7.5 18 L 28.5 18" fill="none" stroke="#000000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_npgp8p =
    '<svg viewBox="91.0 128.7 2.6 1.9" ><path transform="translate(0.0, -36.19)" d="M 91 165.099609375 C 91.25175476074219 165.0356903076172 91.5015869140625 164.9285430908203 91.75583648681641 164.9166564941406 C 92.34147644042969 164.88916015625 92.92921447753906 164.9079437255859 93.60733795166016 164.9079437255859 C 93.261474609375 165.5430297851562 92.95384216308594 166.12646484375 92.62197875976562 166.6957092285156 C 92.57607269287109 166.7743835449219 92.41143035888672 166.8160705566406 92.30026245117188 166.8189544677734 C 91.78209686279297 166.8324584960938 91.35707092285156 166.6144409179688 91 166.2498931884766 L 91 165.099609375 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_n0kltr =
    '<svg viewBox="111.4 128.7 2.6 1.9" ><path transform="translate(-192.36, -36.04)" d="M 306.35693359375 166.0934448242188 C 306.2769775390625 166.1643676757812 306.1971740722656 166.2353820800781 306.1172485351562 166.3061981201172 C 305.4888916015625 166.8629760742188 304.8374938964844 166.7617950439453 304.4487915039062 166.0446624755859 C 304.2305908203125 165.6422729492188 304.0164794921875 165.2374572753906 303.7449951171875 164.7299194335938 C 304.6687316894531 164.8585357666016 305.5303649902344 164.5356750488281 306.35693359375 164.9431610107422 L 306.35693359375 166.0934448242188 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_ycxaaf =
    '<svg viewBox="91.9 124.9 21.2 17.3" ><path transform="translate(-8.67, 0.0)" d="M 117.4001922607422 140.2383575439453 L 104.9413833618164 140.2383575439453 C 104.9413833618164 140.5720520019531 104.9497222900391 140.886962890625 104.9395599365234 141.2012939453125 C 104.9214477539062 141.7582702636719 104.5597763061523 142.119384765625 103.9991455078125 142.1318359375 C 103.5045547485352 142.1427612304688 103.0092926025391 142.1441040039062 102.5147857666016 142.1312561035156 C 101.9577026367188 142.1168975830078 101.5596160888672 141.7480163574219 101.5980529785156 141.1871032714844 C 101.6761474609375 140.0489959716797 101.4159622192383 139.0011596679688 101.031005859375 137.9345245361328 C 100.3134002685547 135.9463653564453 100.4729614257812 133.9784240722656 101.4975128173828 132.1104431152344 C 102.4250869750977 130.4192810058594 103.3907012939453 128.7486114501953 104.3029479980469 127.0492095947266 C 104.9236526489258 125.8928833007812 105.8002395629883 125.2332611083984 107.1478500366211 125.0956420898438 C 110.0349273681641 124.8007659912109 112.9083862304688 124.7620391845703 115.783203125 125.2023010253906 C 116.6593170166016 125.3365631103516 117.2854766845703 125.8033752441406 117.7074432373047 126.5510559082031 C 118.8510131835938 128.5777435302734 119.9883728027344 130.6080627441406 121.1054992675781 132.6494140625 C 121.3014831542969 133.0074462890625 121.417724609375 133.4179992675781 121.5188293457031 133.8175201416016 C 121.8815460205078 135.2504272460938 121.8206024169922 136.6844787597656 121.2777099609375 138.0495300292969 C 120.8798980712891 139.0496520996094 120.6855621337891 140.0463104248047 120.7453460693359 141.1159057617188 C 120.7817687988281 141.7655639648438 120.3968048095703 142.124267578125 119.7539520263672 142.1329803466797 C 119.2911834716797 142.1393127441406 118.8280181884766 142.1413269042969 118.365234375 142.1324157714844 C 117.7743225097656 142.1211090087891 117.4168701171875 141.7641296386719 117.4015350341797 141.1765747070312 C 117.3936767578125 140.876220703125 117.4001922607422 140.5756072998047 117.4001922607422 140.2383575439453 Z M 117.8771514892578 130.6134338378906 C 117.3572540283203 129.6343994140625 116.8875885009766 128.7519683837891 116.4197235107422 127.8684844970703 C 116.1435394287109 127.3470458984375 115.708740234375 127.0872497558594 115.1267395019531 127.00244140625 C 112.8568420410156 126.6717071533203 110.5823211669922 126.6087493896484 108.3037872314453 126.8729705810547 C 106.2801742553711 127.1075592041016 106.2757720947266 127.1170501708984 105.3303680419922 128.9122924804688 C 105.0465087890625 129.4514617919922 104.8052978515625 130.0132293701172 104.5159759521484 130.6249389648438 C 108.988525390625 131.2158355712891 113.3569946289062 131.224365234375 117.8771514892578 130.6134338378906 Z M 111.1554107666016 136.3831787109375 C 112.1910858154297 136.3831787109375 113.2267456054688 136.3814544677734 114.2624206542969 136.3841400146484 C 114.6231384277344 136.3850860595703 114.985107421875 136.3365936279297 114.9720611572266 135.8837890625 C 114.9600830078125 135.4666290283203 114.6111602783203 135.4232177734375 114.2691345214844 135.4235992431641 C 112.1977844238281 135.4259948730469 110.1264495849609 135.4258117675781 108.0551071166992 135.4237060546875 C 107.7093353271484 135.4234008789062 107.3717193603516 135.4797515869141 107.3685531616211 135.9000854492188 C 107.3654861450195 136.3214569091797 107.7024383544922 136.3842315673828 108.0484008789062 136.3836517333984 C 109.0840606689453 136.3819274902344 110.1197357177734 136.3829803466797 111.1554107666016 136.3831787109375 Z M 105.4042587280273 134.9441528320312 C 105.4035873413086 134.1608123779297 104.7532653808594 133.5167999267578 103.9701080322266 133.5241851806641 C 103.2060394287109 133.5313720703125 102.5747833251953 134.1584930419922 102.5624237060547 134.9226684570312 C 102.5496673583984 135.7054443359375 103.1895446777344 136.3606567382812 103.9725036621094 136.3666839599609 C 104.7549896240234 136.3727264404297 105.405029296875 135.7269134521484 105.4042587280273 134.9441528320312 Z M 118.3644714355469 136.3666839599609 C 119.1472320556641 136.3633422851562 119.7895050048828 135.7098541259766 119.7792510986328 134.9271850585938 C 119.7693939208984 134.1630096435547 119.1399536132812 133.5337677001953 118.3759765625 133.5241851806641 C 117.593017578125 133.514404296875 116.9404754638672 134.1563873291016 116.9374237060547 134.9397430419922 C 116.9342498779297 135.7231750488281 117.5811309814453 136.3700561523438 118.3644714355469 136.3666839599609 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_w95fk7 =
    '<svg viewBox="0.0 0.0 7.9 12.2" ><path transform="translate(-87.16, -28.87)" d="M 95.05779266357422 41.072265625 L 87.16199493408203 41.072265625 L 87.16199493408203 39.89669036865234 L 90.32517242431641 36.70925140380859 C 91.28868865966797 35.733642578125 91.92494964599609 35.04283142089844 92.22795104980469 34.62468719482422 C 92.53700256347656 34.20656967163086 92.76724243164062 33.80058670043945 92.92478942871094 33.40668106079102 C 93.07630157470703 33.0128059387207 93.15509796142578 32.58861541748047 93.15509796142578 32.13414001464844 C 93.15509796142578 31.4918098449707 92.96117401123047 30.9827995300293 92.56728363037109 30.60708808898926 C 92.17942810058594 30.23137664794922 91.64013671875 30.04351997375488 90.94934844970703 30.04351997375488 C 90.45244598388672 30.04351997375488 89.97976684570312 30.12835502624512 89.53136444091797 30.29197120666504 C 89.0889892578125 30.4555835723877 88.58604431152344 30.75251388549805 88.04065704345703 31.18881988525391 L 87.31954956054688 30.255615234375 C 88.42241668701172 29.34059906005859 89.62831115722656 28.87399673461914 90.93114471435547 28.87399673461914 C 92.06434631347656 28.87399673461914 92.94902038574219 29.16487503051758 93.59136199951172 29.74661064147949 C 94.23371124267578 30.32228660583496 94.54879760742188 31.09790992736816 94.54879760742188 32.07355117797852 C 94.54879760742188 32.83708190917969 94.33673858642578 33.58848571777344 93.90647125244141 34.33383941650391 C 93.48233032226562 35.07918167114258 92.68242645263672 36.02449798583984 91.50680541992188 37.16371917724609 L 88.87689971923828 39.73910903930664 L 88.87689971923828 39.80578231811523 L 95.05779266357422 39.80578231811523 L 95.05779266357422 41.072265625 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_vsdpfk =
    '<svg viewBox="10.4 0.2 4.3 12.0" ><path transform="translate(-78.52, -28.73)" d="M 93.20269775390625 40.92553329467773 L 91.86952972412109 40.92553329467773 L 91.86952972412109 32.3570442199707 C 91.86952972412109 31.64201545715332 91.89381408691406 30.96936988830566 91.93620300292969 30.33309555053711 C 91.82108306884766 30.4482479095459 91.687744140625 30.56943511962891 91.54834747314453 30.69668006896973 C 91.40903472900391 30.82394027709961 90.75453948974609 31.35721397399902 89.59107208251953 32.29645156860352 L 88.8699951171875 31.3632698059082 L 92.05130767822266 28.90299797058105 L 93.20269775390625 28.90299797058105 L 93.20269775390625 40.92553329467773 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_r5m4w =
    '<svg viewBox="19.7 3.0 2.0 9.5" ><path transform="translate(-70.73, -26.4)" d="M 90.41000366210938 30.47092819213867 C 90.41000366210938 29.73165130615234 90.73112487792969 29.36199569702148 91.37953948974609 29.36199569702148 C 92.05216979980469 29.36199569702148 92.39150238037109 29.73165130615234 92.39150238037109 30.47092819213867 C 92.39150238037109 30.82845306396484 92.30062866210938 31.10114288330078 92.11880493164062 31.29505920410156 C 91.93702697753906 31.48291015625 91.69465637207031 31.57987594604492 91.37953948974609 31.57987594604492 C 91.10079193115234 31.57987594604492 90.87052154541016 31.49504280090332 90.68270111083984 31.31929397583008 C 90.50088500976562 31.14962959289551 90.41000366210938 30.86482429504395 90.41000366210938 30.47092819213867 Z M 90.41000366210938 37.73049926757812 C 90.41000366210938 37.36086654663086 90.49481964111328 37.08211517333984 90.65839385986328 36.89425277709961 C 90.82806396484375 36.70640563964844 91.07048034667969 36.61551666259766 91.37953948974609 36.61551666259766 C 91.70072174072266 36.61551666259766 91.94914245605469 36.70640563964844 92.12486267089844 36.89425277709961 C 92.30062866210938 37.08211517333984 92.39150238037109 37.36086654663086 92.39150238037109 37.73049926757812 C 92.39150238037109 38.0880241394043 92.30062866210938 38.3607177734375 92.11880493164062 38.5485725402832 C 91.93702697753906 38.74248123168945 91.69465637207031 38.83944320678711 91.37953948974609 38.83944320678711 C 91.10079193115234 38.83944320678711 90.87052154541016 38.75461578369141 90.68270111083984 38.5788688659668 C 90.50088500976562 38.40919494628906 90.41000366210938 38.12439346313477 90.41000366210938 37.73049926757812 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_k841h =
    '<svg viewBox="23.7 0.0 7.9 12.4" ><path transform="translate(-67.39, -28.87)" d="M 98.57193756103516 31.87962913513184 C 98.57193756103516 32.64922332763672 98.35988616943359 33.27338409423828 97.92352294921875 33.76420974731445 C 97.49327850341797 34.24900436401367 96.88734436035156 34.57622909545898 96.09352111816406 34.73982238769531 L 96.09352111816406 34.80648803710938 C 97.06307220458984 34.92767715454102 97.77812957763672 35.23066329956055 98.2386474609375 35.72756576538086 C 98.71133422851562 36.21841049194336 98.94162750244141 36.86681365966797 98.94162750244141 37.66669845581055 C 98.94162750244141 38.81197738647461 98.54164886474609 39.69670104980469 97.747802734375 40.30873107910156 C 96.95399475097656 40.92684173583984 95.82688903808594 41.23588180541992 94.36041259765625 41.23588180541992 C 93.72417449951172 41.23588180541992 93.1424560546875 41.18740081787109 92.61518859863281 41.09043502807617 C 92.08196258544922 40.99348449707031 91.57295989990234 40.82987594604492 91.06999206542969 40.58747863769531 L 91.06999206542969 39.28463745117188 C 91.59111022949219 39.54522323608398 92.14861297607422 39.73910903930664 92.73642730712891 39.87850570678711 C 93.32423400878906 40.00574493408203 93.88778686523438 40.07847213745117 94.40890502929688 40.07847213745117 C 96.48738098144531 40.07847213745117 97.52968597412109 39.26039123535156 97.52968597412109 37.63032150268555 C 97.52968597412109 36.17599868774414 96.37833404541016 35.44276809692383 94.08772277832031 35.44276809692383 L 92.90608978271484 35.44276809692383 L 92.90608978271484 34.27322006225586 L 94.10589599609375 34.27322006225586 C 95.04515838623047 34.27322006225586 95.78445434570312 34.0611457824707 96.33589935302734 33.64907836914062 C 96.88128662109375 33.23701858520508 97.15998840332031 32.66134262084961 97.15998840332031 31.9281177520752 C 97.15998840332031 31.34030914306641 96.95399475097656 30.8797779083252 96.55405426025391 30.54649353027344 C 96.14802551269531 30.21319389343262 95.60267639160156 30.04351997375488 94.91188812255859 30.04351997375488 C 94.38465118408203 30.04351997375488 93.88778686523438 30.11624717712402 93.42115783691406 30.255615234375 C 92.95454406738281 30.40104866027832 92.4273681640625 30.6616268157959 91.82743835449219 31.04945182800293 L 91.13665771484375 30.12835502624512 C 91.63356018066406 29.74055099487305 92.19709777832031 29.43148994445801 92.84549713134766 29.2072811126709 C 93.48784637451172 28.9891300201416 94.16653442382812 28.87399673461914 94.88154602050781 28.87399673461914 C 96.04505920410156 28.87399673461914 96.95399475097656 29.1406307220459 97.60240173339844 29.67993927001953 C 98.24470520019531 30.21319389343262 98.57193756103516 30.94643020629883 98.57193756103516 31.87962913513184 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_s7du =
    '<svg viewBox="32.9 0.1 8.9 12.1" ><path transform="translate(-59.7, -28.78)" d="M 101.5281600952148 38.21794891357422 L 99.740478515625 38.21794891357422 L 99.740478515625 40.98119354248047 L 98.43766784667969 40.98119354248047 L 98.43766784667969 38.21794891357422 L 92.59000396728516 38.21794891357422 L 92.59000396728516 37.02416229248047 L 98.29827117919922 28.89199829101562 L 99.740478515625 28.89199829101562 L 99.740478515625 36.97570037841797 L 101.5281600952148 36.97570037841797 L 101.5281600952148 38.21794891357422 Z M 98.43766784667969 36.97570037841797 L 98.43766784667969 32.97626495361328 C 98.43766784667969 32.19456481933594 98.46794128417969 31.30984497070312 98.51645660400391 30.32210540771484 L 98.45582580566406 30.32210540771484 C 98.18922424316406 30.84929847717285 97.94682312011719 31.28560447692871 97.71050262451172 31.63100242614746 L 93.9534912109375 36.97570037841797 L 98.43766784667969 36.97570037841797 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_kwb1 =
    '<svg viewBox="0.9 0.9 13.9 11.6" ><path transform="translate(-81.14, -28.05)" d="M 95.94999694824219 28.96000099182129 L 92.21902465820312 40.56137084960938 L 82.04349517822266 40.56137084960938 L 82.04349517822266 28.96000099182129 L 95.94999694824219 28.96000099182129 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_b8wx2f =
    '<svg viewBox="0.0 0.0 15.7 13.4" ><path transform="translate(-81.89, -28.81)" d="M 92.97221374511719 42.22627639770508 L 82.79788970947266 42.22627639770508 C 82.29492950439453 42.22627639770508 81.89499664306641 41.82027053833008 81.89499664306641 41.31730651855469 L 81.89499664306641 29.71896171569824 C 81.89499664306641 29.21600723266602 82.29492950439453 28.80999946594238 82.79788970947266 28.80999946594238 L 96.70500183105469 28.80999946594238 C 96.99589538574219 28.80999946594238 97.26248931884766 28.94937324523926 97.43215942382812 29.18571281433105 C 97.60787963867188 29.42203140258789 97.65640258789062 29.71896171569824 97.56549072265625 29.99770736694336 L 93.83267974853516 41.5960578918457 C 93.71150207519531 41.97177124023438 93.36610412597656 42.22627639770508 92.97221374511719 42.22627639770508 Z M 83.70082092285156 40.41440582275391 L 92.31166839599609 40.41440582275391 L 95.46278381347656 30.62185096740723 L 83.70082092285156 30.62185096740723 L 83.70082092285156 40.41440582275391 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_szaib7 =
    '<svg viewBox="0.0 0.0 23.6 13.4" ><path transform="translate(-81.89, -28.81)" d="M 104.6190185546875 42.22627639770508 L 82.79788970947266 42.22627639770508 C 82.29492950439453 42.22627639770508 81.89499664306641 41.82027053833008 81.89499664306641 41.31730651855469 L 81.89499664306641 29.71896171569824 C 81.89499664306641 29.21600723266602 82.29492950439453 28.80999946594238 82.79788970947266 28.80999946594238 L 104.6190185546875 28.80999946594238 C 105.1159210205078 28.80999946594238 105.5219421386719 29.21600723266602 105.5219421386719 29.71896171569824 L 105.5219421386719 41.31730651855469 C 105.5219421386719 41.82027053833008 105.1159210205078 42.22627639770508 104.6190185546875 42.22627639770508 Z M 83.70082092285156 40.41440582275391 L 103.7161560058594 40.41440582275391 L 103.7161560058594 30.62185096740723 L 83.70082092285156 30.62185096740723 L 83.70082092285156 40.41440582275391 Z" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
*/
