import 'dart:developer';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:navtaxidriver/api/api.dart';
import 'package:navtaxidriver/model/shared_pre.dart';
import 'package:navtaxidriver/model/user_response.dart';
import 'package:navtaxidriver/router.dart';
import 'package:navtaxidriver/util/constants.dart';
import 'package:navtaxidriver/util/util.dart';
import 'package:provider/provider.dart';

import 'login.dart';

List<String> otp = [
  "",
  "",
  "",
  "",
  "",
  "",
];

class OTPScreen extends StatefulWidget {
  final String _phone;
  final String _countryCode;
  String _verificationId = "";
  int _ftr = 0;

  OTPScreen(this._phone, this._countryCode,
      /*this._verificationId, this._ftr,*/
      {Key? key})
      : super(key: key);

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final List<FocusNode?> nodes = [
    null,
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
    null
  ];

  Future<UserCredential>? _user;
  Future<UserResponse>? _futureUser;

  Future<void>? _futureResend;
  bool isApiCall = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // log(widget._verificationId);
    otp = [
      "",
      "",
      "",
      "",
      "",
      "",
    ];
    Api.verifyPhone(
        widget._countryCode + widget._phone, context, codeResend, autoRetrieval, onError);
  }

  onError(error) {
    showSnackBar(">>>  ${error.message}");
    setState(() {
      isApiCall = false;
    });
  }

  codeResend(String verificationId, int? forceResendingToken) {
    widget._verificationId = verificationId;
    widget._ftr = forceResendingToken!;
  }

  autoRetrieval(String verificationId) {
    widget._verificationId = verificationId;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: getMainBackground(
          context,
          Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  backButton(context),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        getSizeBox(10),
                        getText("ENTER OTP",
                            style: const TextStyle(
                              fontSize: 40,
                              color: Color(0xff000000),
                              fontWeight: FontWeight.w700,
                            )),
                        getText(
                          'OTP has been sent to',
                          style: const TextStyle(
                            fontSize: 16,
                            color: Color(0xff373737),
                          ),
                        ),
                        getText(widget._countryCode + widget._phone,
                            style: const TextStyle(
                              fontSize: 20,
                              color: Color(0xff000000),
                              fontWeight: FontWeight.w600,
                            )),
                        getSizeBox(10),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: getWidth(context) / 20, vertical: 20),
                          child: Column(
                            children: [
                              Row(
                                children: List.generate(
                                    otp.length,
                                    (index) => Expanded(
                                            child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextEditorForPhoneVerify(
                                              index,
                                              otp[index],
                                              nodes[index],
                                              nodes[index + 1],
                                              nodes[index + 2]),
                                        ))),
                              ),
                              FutureBuilder(
                                future: _futureResend,
                                builder: (context, snapshot) {
                                  log("$snapshot");
                                  if (snapshot.connectionState == ConnectionState.none ||
                                      snapshot.hasData ||
                                      snapshot.connectionState == ConnectionState.done) {
                                    return Align(
                                        alignment: Alignment.topRight,
                                        child: TextButton(
                                            onPressed: () {
                                              setState(() {
                                                _futureResend = Api.verifyPhone(
                                                    widget._countryCode+widget._phone,
                                                    context,
                                                    codeResend,
                                                    autoRetrieval,
                                                    onError,
                                                    ftr: widget._ftr);
                                              });
                                            },
                                            child: getText("Resend otp",
                                                style: const TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black))));
                                  }
                                  return Align(
                                      alignment: Alignment.topRight, child: getLoading());
                                },
                              ),
                            ],
                          ),
                        ),
                        verifyButton(),
                        /*FutureBuilder<UserCredential>(
                          future: _user,
                          builder: (ctx, snapshot) {
                            log("$snapshot");
                            if (snapshot.hasData) {
                              log(">>>>>>>>>>>>>>>>${snapshot.data!.user}");
                              log(">>>>>>>>>>>>>>>>${snapshot.data!.credential}");
                              log(">>>>>>>>>>>>>>>>${snapshot.data}");
                              SharedPrefer sp =
                                  Provider.of<SharedPrefer>(context, listen: false);
                              sp.saveString(
                                  keyPhoneNumber, snapshot.data!.user!.phoneNumber!);
                              // sp.saveSession(widget._phone, UserResponse());
                              // openUntil(context, registerScreenRoute);
                              // return verifyButton();
                              setState(() {
                                _futureUser = Api.callPhoneApi(widget._phone,
                                    widget._countryCode, LoginScreen.deviceToken!);
                              });

                              */
                        /*setState(() {
                                _user=null;
                              });*/
                        /*
                            }
                            if (snapshot.connectionState == ConnectionState.none ||
                                snapshot.connectionState == ConnectionState.done) {
                              return verifyButton();
                            }

                            if (snapshot.hasError) {
                              FirebaseAuthException exp =
                                  snapshot.error as FirebaseAuthException;
                              showSnackBar("${exp.message}");
                              log(">>>>>>>>ERROR>>>>>>>>${snapshot.error}");
                            }
                            return getLoading();
                          },
                        )*/
                      ],
                    ),
                  ),
                ],
              ),
              isApiCall ? baseLoader(context) : getSizeBox(1),
              /*FutureBuilder<UserResponse>(
                future: _futureUser,
                builder: (ctx, snapshot) {
                  if (snapshot.hasError) {
                    // showSnackBar(snapshot.data!.message??"Something went wrong! please try again");
                    return getSizeBox(1);
                  }
                  if (snapshot.hasData) {
                    if (snapshot.data!.success ?? false) {
                      // showSnackBar(snapshot.data!.message??"Something went wrong! please try again");
                      return getSizeBox(1);
                    } else {
                      // return  baseLoader(context);
                      showSnackBar(snapshot.data!.message ??
                          "Something went wrong! please try again");
                      return Container();
                    }
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return baseLoader(context);
                  }
                  return getSizeBox(1);
                },
              )*/
            ],
          )),
    ));
  }

  Widget verifyButton() {
    return getButton("Verify", () {
      String code = "";
      for (var element in otp) {
        if (element.isNotEmpty) {
          code += element;
        }
      }
      log(code);
      if (code.isEmpty) {
        showSnackBar("Please enter otp code");
      } else if (code.length != 6) {
        showSnackBar("Please enter 6 digit code");
      } else {
        hideKeyboard(context);
        PhoneAuthCredential credential = PhoneAuthProvider.credential(
            verificationId: widget._verificationId, smsCode: code);
        setState(() {
          isApiCall = true;
        });
        callApi(credential);
      }
    });
  }

  Future<void> callApi(PhoneAuthCredential credential) async {
    try {
      // FirebaseAuth.instance.signInWithPhoneNumber("",RecaptchaVerifier());
      // FirebaseAuth.instance.verifyPhoneNumber(phoneNumber: phoneNumber, verificationCompleted: verificationCompleted, verificationFailed: verificationFailed, codeSent: codeSent, codeAutoRetrievalTimeout: codeAutoRetrievalTimeout)
      UserCredential cre=await FirebaseAuth.instance.signInWithCredential(credential);
      UserResponse response = await Api.callPhoneApi(
          widget._phone, widget._countryCode, LoginScreen.deviceToken??"");
      if (response.success ?? false) {
        SharedPrefer sp = Provider.of<SharedPrefer>(context, listen: false);
        log("OTP Screen 261 ${response}");
        await sp.saveSession(response.data!.details);
        setState(() {
          isApiCall = false;
        });
        openUntil(context, splashScreenRoute,arguments: SplashArgument(true));
      } else {
        showSnackBar(response.message ?? "Something went wrong");
        setState(() {
          isApiCall = false;
        });
      }
    } on FirebaseAuthException catch (e) {
      log("<>  ${e}");
      log("<><OTP><> ${e.credential}");
      setState(() {
        isApiCall = false;
      });
      showSnackBar("${e.message}");
    }
  }
}

class TextEditorForPhoneVerify extends StatelessWidget {
  late TextEditingController _controller;
  final int index;
  final String value;
  final FocusNode? _focusNode;
  final FocusNode? _secondNode;
  final FocusNode? _previousNode;

  TextEditorForPhoneVerify(
      this.index, this.value, this._previousNode, this._focusNode, this._secondNode,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _controller = TextEditingController(text: value);
    return TextField(
        focusNode: _focusNode,
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        controller: _controller,
        maxLength: 1,
        onChanged: (value) {
          if (value.isNotEmpty && _secondNode != null) {
            _secondNode!.requestFocus();
          } else if (value.isEmpty && _previousNode != null) {
            _previousNode!.requestFocus();
          }
          otp[index] = value;
        },
        onSubmitted: (value) {
          if (value.isNotEmpty && _secondNode != null) {
            _secondNode!.requestFocus();
          } else if (value.isEmpty && _previousNode != null) {
            _previousNode!.requestFocus();
          } else {
            hideKeyboard(context);
          }
          otp[index] = value;
        },
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide.none, borderRadius: BorderRadius.circular(15)),
            fillColor: const Color(0xffdcd7d7),
            filled: true,
            counterText: '',
            hintText: "0",
            hintStyle: TextStyle(fontSize: 20)));
  }
}

/*
*
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextEditorForPhoneVerify(
                                "1", null, _currentNode, _firstNode),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextEditorForPhoneVerify(
                                "1", _currentNode, _firstNode, _secondNode),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextEditorForPhoneVerify(
                                "1", _firstNode, _secondNode, _thirdNode),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextEditorForPhoneVerify(
                                "1", _secondNode, _thirdNode, _fourthNode),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextEditorForPhoneVerify(
                                "1", _thirdNode, _fourthNode, _fifthNode),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextEditorForPhoneVerify(
                                "1", _fourthNode, _fifthNode, null),
                          )*/
