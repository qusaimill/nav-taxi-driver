import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:navtaxidriver/src/home/history.dart';
import 'package:navtaxidriver/util/constants.dart';
import 'package:navtaxidriver/util/util.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Widget currentWidget = const HomeTab();
  final GlobalKey<ScaffoldState> _key = GlobalKey();


  /*_getAddressFromLatLng() async {
    try {
      Geolocator.List<Placemark> p = await Geo.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      Placemark place = p[0];
      setState(() {
        _currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }*/

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            key: _key,
            drawer: Drawer(
              child: ListView(
                children: [
                  DrawerHeader(
                    child: Center(
                      child: Column(
                        children: [
                          const ClipOval(
                            child: Icon(
                              Icons.account_circle,
                              color: Colors.white,
                              size: 55,
                            ),
                          ),
                          getSizeBox(10),
                          getText("Person Name",
                              style: const TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                    decoration: BoxDecoration(color: yellow_accent),
                  ),
                  getTile("Home", () {
                    setState(() {
                      currentWidget = const HomeTab();
                      Navigator.pop(context);
                    });
                  }, Icons.home),
                  getTile("Profile", () {}, Icons.account_circle),
                  getTile("Order History", () {
                    setState(() {
                      currentWidget = const HistoryTab();
                      Navigator.pop(context);
                    });
                  }, Icons.history),
                  getTile("Logout", () {}, Icons.logout)
                ],
              ),
            ),
            body: Stack(
              children: [
                currentWidget,
                getMenu(() {
                  _key.currentState!.openDrawer();
                })
              ],
            )));
  }

  getTile(String s, onPress, IconData icon) {
    return ListTile(
      onTap: onPress,
      leading: Icon(
        icon,
        color: Colors.black,
      ),
      title: getText(s, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
    );
  }
}

class HomeTab extends StatefulWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  late GoogleMapController mapController;
  final Map<String, Marker> _markers = {};

  LatLng? _center;
  late Position _currentPosition;
  String _currentAddress = "";
  late LocationSettings locationSettings;

  _getCurrentLocation() async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      locationSettings = AndroidSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 100,
        forceLocationManager: true,
        intervalDuration: const Duration(seconds: 10),
      );
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      locationSettings = AppleSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 100,
        pauseLocationUpdatesAutomatically: true,
      );
    } else {
      locationSettings = const LocationSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 100,
      );
    }

    LocationPermission locationPermission=await Geolocator.checkPermission();
    if(locationPermission==LocationPermission.denied){
      await  Geolocator.requestPermission();
    }
    _currentPosition =
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      _center=LatLng(_currentPosition.latitude, _currentPosition.longitude);
      log("$_center");
    });
    // _getAddressFromLatLng();
  }
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _markers.clear();
    var marker = Marker(
      markerId: const MarkerId("Home"),
      position: _center!,
    );
    _markers["Home"] = marker;
    setState(() {});
  }
@override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: _center!=null?getMap():baseLoader(context),
    );
  }

  getMap() {

    return GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center!,
        zoom: 15.0,
      ),
      markers: _markers.values.toSet(),
    );
  }
}
