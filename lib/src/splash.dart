import 'dart:developer';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:navtaxidriver/api/api.dart';
import 'package:navtaxidriver/model/shared_pre.dart';
import 'package:navtaxidriver/model/user_response.dart';
import 'package:navtaxidriver/router.dart';
import 'package:navtaxidriver/src/login.dart';
import 'package:navtaxidriver/util/constants.dart';
import 'package:navtaxidriver/util/util.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key, this.isFromOTP = false}) : super(key: key);
  final bool isFromOTP;

  @override
  Widget build(BuildContext context) {
    manageSession(context);
    return SafeArea(
      child: Scaffold(body: getMainBackground(context, baseLoader(context))),
    );
  }

  Future<void> manageSession(BuildContext context) async {
    SharedPrefer prefer = Provider.of<SharedPrefer>(context, listen: false);
    bool isLogin = await prefer.getBool(keyLogin);
    if (isLogin) {
      if(isFromOTP){
        Details? user = await prefer.getUser();
        openScreen(context,user);
        return;
      }else{
        String id = await prefer.getString(keyDriverId);
        String authToken = await prefer.getString(keyToken);

        ///--------------After driver verified there number-------------------///
        if (id.isNotEmpty && authToken.isNotEmpty) {
          Api.authToken = authToken;
          UserResponse response = await Api.callSessionApi(id);
          bool isSuccess = response.success ?? false;

          ///--------------Check Driver is Details-------------------///
          if (isSuccess && response.data!.details != null) {
            Details? user = response.data!.details!;
            prefer.saveSession(user);
            openScreen(context,user);
            return;
          }
        }
      }

    }
    openUntil(context, loginScreenRoute);

    /*if (isLogin) {
      String _deviceToken = await prefer.getString(keyDeviceToken);
      if(_deviceToken.isEmpty){
        _deviceToken = await FirebaseMessaging.instance.getToken()??"";
        LoginScreen.deviceToken=_deviceToken;
      }
      log("Device Token $_deviceToken");

      log("is Login");
      Details? user = await prefer.getUser();
      log("user $user");

      if (user.isVerify() && user.status!.isNotEmpty) {
        Api.authToken = user.apiKey ?? "";
        String status = user.status!.toUpperCase();
        if (status == "APPROVED") {
          openUntil(context, homeScreenRoute);
        } else if (status == "PENDING"||status == "DECLINE") {
          openUntil(context, approvalScreenRoute);
        } else if(status=="ONBOARDING") {
          openUntil(context, registerScreenRoute,
              arguments: RegisterScreenArgument("${user.countryCode!}${user.mobile!}"));
        }
      } else {
        openUntil(context, registerScreenRoute,
            arguments: RegisterScreenArgument("${user.countryCode!}${user.mobile!}"));
      }
    } else {
      openUntil(context, loginScreenRoute);
    }*/

    /* bool isLogin = await prefer.getBool(keyLogin);
    if (isLogin) {
      UserResponse user = await prefer.getUser();
    }*/
  }

  void openScreen(BuildContext context,Details user) {
    if (user.isVerify()) {
      String status = user.status!.toUpperCase();
      log("status $status");
      ///--------------Check Driver is Status-------------------///
      if (status == "APPROVED") {
        openUntil(context, homeScreenRoute);
        return;

      } else if (status == "PENDING") {//pending
        openUntil(context, approvalScreenRoute,
            arguments: ApprovalScreenArgument(
                user.message!.isNotEmpty ? user.message![0] : ""));
        return;
      } else if (status == "BANNED") {
        openUntil(context, loginScreenRoute);
        return;

      }
    }
    openUntil(context, registerScreenRoute,
        arguments: RegisterScreenArgument("${user.countryCode!}${user.mobile!}"));
  }
}
/* else if (status == "ONBOARDING" ) {
        openUntil(context, registerScreenRoute,
            arguments:
            RegisterScreenArgument("${user.countryCode!}${user.mobile!}"));
      }*/