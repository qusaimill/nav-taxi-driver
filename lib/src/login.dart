import 'dart:developer';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:navtaxidriver/api/api.dart';
import 'package:navtaxidriver/model/login_model.dart';
import 'package:navtaxidriver/model/shared_pre.dart';
import 'package:navtaxidriver/router.dart';
import 'package:navtaxidriver/util/constants.dart';
import 'package:navtaxidriver/util/util.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);
  TextEditingController _phoneController = TextEditingController();

  Future<void>? _login;
  static String? deviceToken;

  @override
  Widget build(BuildContext context) {
    LoginModel model = Provider.of<LoginModel>(context, listen: false);
    if (model.phone.isNotEmpty) {
      _phoneController = TextEditingController(text: model.phone);
    }
    initDeviceToken(context);
    return SafeArea(
      child: Scaffold(
        body: getMainBackground(
            context,
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getText("Enter Phone", style: getTitleStyle()),
                  getSizeBox(20),
                  getImage("ic_phone.png"),
                  getSizeBox(10),
                  getPhoneTextField(),
                  getSizeBox(20),
                  FutureBuilder(
                    future: _login,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.none) {
                        log("$snapshot");
                        return getButton("LOGIN", () {
                          bool isValid = model.changePhoneNumber(_phoneController.text);
                          if (isValid) {
                            // String phone=_phoneController.text;
                            openOtpScreen(context, _phoneController.text);
                          }
                        });
                      }
                      return getLoading();
                    },
                  ),
                ],
              ),
            )),
      ),
    );
  }

  Widget getPhoneTextField() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: Colors.white,
      elevation: 10,
      margin: const EdgeInsets.all(15),
      child: Consumer<LoginModel>(
        builder: (context, loginModel, child) => TextFormField(
          keyboardType: TextInputType.phone,
          controller: _phoneController,
          onFieldSubmitted: (value) {
            bool isValid = loginModel.changePhoneNumber(_phoneController.text);
            if (isValid) {
              hideKeyboard(context);
              openOtpScreen(context, loginModel.phone);
            }
          },
          decoration: InputDecoration(
              prefixIcon: const Icon(Icons.phone),
              hintText: "(650) 555-1212",
              prefixText: "+91",
              // errorText: loginModel.phone.error,
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(20),
              ),
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(20))),
        ),
      ),
    );
  }

  openOtpScreen(BuildContext context, String phone) {
    String countryCode = "+91";
    hideKeyboard(context);
    /*_login = Api.verifyPhone(phone.contains(countryCode) ? phone : countryCode + phone, context,
        (verificationId, forceResendingToken) {
      log(phone);*/
    open(context, otpScreenRoute, arguments: OtpScreenArguments(phone, countryCode));
    // });
  }

  Future<void> initDeviceToken(BuildContext context) async {
    var sp = Provider.of<SharedPrefer>(context, listen: false);
    String _deviceToken = await sp.getString(keyDeviceToken);
    Api.authToken = await sp.getString(keyToken);
    if (_deviceToken.isEmpty) {
      log("Generate New Token");
      deviceToken = await FirebaseMessaging.instance.getToken();
      sp.saveString(keyDeviceToken, deviceToken ?? "");
    } else {
      deviceToken = _deviceToken;
    }
    log("Device Token $deviceToken");
    log("Auth Token ${Api.authToken}");
    // Clipboard.setData(ClipboardData(text: deviceToken));
  }
}
